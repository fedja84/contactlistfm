﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContactListSrvc.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ContactListSrvc.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ContactsController : Controller
    {
        ContactContext db;

        public ContactsController(ContactContext context)
        {
            db = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Contact> Get()
        {
            Startup.logger.LogDebug("Getting contacts");
            return db.Contacts.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Startup.logger.LogDebug($"Getting one contact id={id}");
            Contact contact = db.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact == null)
            {
                Startup.logger.LogWarning($"Contact not found id={id} (getOneContact)");
                return NotFound();
            }
            else
            {
                return new ObjectResult(contact);
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]Contact contact)
        {
            Startup.logger.LogDebug("Start add contact...");
            if (contact == null)
            {
                Startup.logger.LogWarning($"Can not add contact, because contact is null");
                return BadRequest();
            }
            else
            {
                db.Contacts.Add(contact);
                db.SaveChanges();
                Startup.logger.LogInformation($"New contact added. Contact id:{contact.Id}");
                Startup.logger.LogDebug($"New contact information:{Environment.NewLine}id:{contact.Id}{Environment.NewLine}Name:{contact.Name}{Environment.NewLine}Phone:{contact.PhoneNumber}{Environment.NewLine}Email:{contact.EmailAddress}{Environment.NewLine}Birthday:{contact.Birthday.ToString("yyyy-MM-dd")}");
                return Ok(contact);
            }
        }

        // PUT api/<controller>/5
        [HttpPut]
        public IActionResult Put([FromBody]Contact contact)
        {
            Startup.logger.LogDebug($"Start update contact.");
            if (contact == null)
            {
                Startup.logger.LogWarning($"Can not update contact, because contact is null");
                return BadRequest();
            }
            else
            {
                if (!db.Contacts.Any(x => x.Id == contact.Id))
                {
                    Startup.logger.LogWarning($"Can not update contact, because contact {contact.Id} not found");
                    return NotFound();
                }
                else
                {
                    db.Update(contact);
                    db.SaveChanges();
                    Startup.logger.LogInformation($"Contact changed. Contact id:{contact.Id}");
                    Startup.logger.LogDebug($"Contact after change:{Environment.NewLine}id:{contact.Id}{Environment.NewLine}Name:{contact.Name}{Environment.NewLine}Phone:{contact.PhoneNumber}{Environment.NewLine}Email:{contact.EmailAddress}{Environment.NewLine}Birthday:{contact.Birthday.ToString("yyyy-MM-dd")}");
                    return Ok(contact);
                }
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Startup.logger.LogDebug($"Start delete contact.");
            Contact contact = db.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact == null)
            {
                Startup.logger.LogWarning($"Can not delete contact, because contact is null");
                return NotFound();
            }
            else
            {
                db.Remove(contact);
                db.SaveChanges();
                Startup.logger.LogInformation($"Contact deleted. Contact id:{contact.Id}");
                Startup.logger.LogDebug($"Deleted contact:{Environment.NewLine}id:{contact.Id}{Environment.NewLine}Name:{contact.Name}{Environment.NewLine}Phone:{contact.PhoneNumber}{Environment.NewLine}Email:{contact.EmailAddress}{Environment.NewLine}Birthday:{contact.Birthday.ToString("yyyy-MM-dd")}");
                return Ok(contact);
            }
        }
    }
}
