﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using ContactListSrvc.Models;
using Microsoft.Extensions.Logging;
using System.IO;

namespace ContactListSrvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        //private ILogger logger;
        [AllowAnonymous]
        public ActionResult<string> Post(
            AuthenticationRequest authRequest,
            [FromServices] IJwtSigningEncodingKey signingEncodingKey)
        {
            // 1. Проверяем данные пользователя из запроса.
            /*ILoggerFactory loggerFactory = new LoggerFactory();
            
            int loglevel = 1;
            FileLoggerExtensions.AddFile(loggerFactory, System.IO.Path.Combine(Directory.GetCurrentDirectory(), "logService"), (LogLevel)loglevel);
            logger = loggerFactory.CreateLogger("FileLogger");*/
            Startup.logger.LogDebug($"Authorization request. UserName:{authRequest.Name} lengthPassword:{authRequest.Password.Length}");
            if (identityUser(authRequest.Name, authRequest.Password))
            {
                Startup.logger.LogInformation($"Success authorization. UserName:{authRequest.Name} lengthPassword:{authRequest.Password.Length}");
                // 2. Создаем утверждения для токена.
                var claims = new Claim[]
                {
                new Claim(ClaimTypes.NameIdentifier, authRequest.Name)
                };

                // 3. Генерируем JWT.
                var token = new JwtSecurityToken(
                    issuer: "DemoApp",
                    audience: "DemoAppClient",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: new SigningCredentials(
                            signingEncodingKey.GetKey(),
                            signingEncodingKey.SigningAlgorithm)
                );

                string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
                Startup.logger.LogInformation($"Toket created for user:{authRequest.Name} jwtToken.Length:{jwtToken.Length}");
                return jwtToken;
            }
            else
            {
                Startup.logger.LogWarning($"Fail authorization UserName:{authRequest.Name} lengthPassword:{authRequest.Password.Length}");
                Response.StatusCode = 400;
                return "Invalid username or password.";
            }
        }
        private bool identityUser(string userName, string userPassword)
        {
            return (userName == "fedja" && userPassword == "qwerty");
        }
    }
}
