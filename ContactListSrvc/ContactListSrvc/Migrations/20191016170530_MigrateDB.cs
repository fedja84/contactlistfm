﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactListSrvc.Migrations
{
    public partial class MigrateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*try
            {
                migrationBuilder.CreateTable(
                    name: "Contacts",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        Name = table.Column<string>(nullable: true),
                        PhoneNumber = table.Column<string>(nullable: true),
                        EmailAddress = table.Column<string>(nullable: true),
                        Birthday = table.Column<DateTime>(nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Contacts", x => x.Id);
                    });
            }
            catch { }*/

            migrationBuilder.AddColumn<DateTime>("Birthday", "Contacts", nullable: false, defaultValue:new DateTime(1753,01,01));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");
        }
    }
}
