﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ContactListSrvc.Models
{
    public class FileLoggerProvider : ILoggerProvider
    {
        private string path;
        private LogLevel LogLevel;
        public FileLoggerProvider(string _path, LogLevel logLevel)
        {
            LogLevel = logLevel;
            path = _path;
        }
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger(path, LogLevel);
        }

        public void Dispose()
        {
        }
    }
}
