﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.Logging;

namespace ContactListSrvc.Models
{
    public class FileLogger : ILogger
    {
        private LogLevel needLevel = LogLevel.Trace;
        private string filePath;
        private object _lock = new object();
        public FileLogger(string path, LogLevel logLevel)
        {
            filePath = path;
            needLevel = logLevel;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            if (needLevel == LogLevel.None)
            {
                return false;
            }
            return logLevel >= needLevel;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (IsEnabled(logLevel))
            {
                if (formatter != null)
                {
                    lock (_lock)
                    {
                        File.AppendAllText(filePath, $"{DateTime.Now.ToString("HH:mm:ss.fff")} \t {logLevel} \t {formatter(state, exception)} {Environment.NewLine}");
                    }
                }
            }
        }
    }
}
