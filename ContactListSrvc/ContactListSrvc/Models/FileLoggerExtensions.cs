﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ContactListSrvc.Models
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath, LogLevel logLevel)
        {
            filePath += $"_{DateTime.Now.ToString("yyyy-MM-dd")}.txt";
            factory.AddProvider(new FileLoggerProvider(filePath, logLevel));
            return factory;
        }
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath, string logLevel)
        {
            LogLevel level = LogLevel.Information;
            switch (logLevel)
            {
                case "Trace":
                    level = LogLevel.Trace;
                    break;
                case "Debug":
                    level = LogLevel.Debug;
                    break;
                case "Information":
                    level = LogLevel.Information;
                    break;
                case "Warning":
                    level = LogLevel.Warning;
                    break;
                case "Error":
                    level = LogLevel.Error;
                    break;
                case "Critical":
                    level = LogLevel.Critical;
                    break;
                case "None":
                    level = LogLevel.None;
                    break;
            }
            filePath += $"_{DateTime.Now.ToString("yyyy-MM-dd")}.txt";
            factory.AddProvider(new FileLoggerProvider(filePath, level));
            return factory;
        }
    }
}
