﻿using System;
using System.Collections.Generic;
using System.Text;
using ContactListClient.Classes;
using Xunit;

namespace ContactListClient.Tests
{
    public class JSONSerializTests
    {
        [Fact]
        public void GetJSON()
        {
            JSONSerializ json = new JSONSerializ();
            Contact contact = new Contact("Fede", "+79193443062", "fede@gmail.com");
            json.GetJson(contact);

            Assert.Equal("{\"birthday\":\"0001-01-01\",\"emailAddress\":\"fede@gmail.com\",\"id\":0,\"name\":\"Fede\",\"phoneNumber\":\"+79193443062\"}", json.JsonString);
        }

        [Fact]
        public void ParseJSONNotNULL()
        {
            JSONSerializ json = new JSONSerializ();

            json.ParseJSON("[{\"birthday\":\"0001-01-01\",\"emailAddress\":\"fede@gmail.com\",\"id\":0,\"name\":\"Fede\",\"phoneNumber\":\"+79193443062\"}]");
            Assert.NotNull(json.Contacts);
        }

        [Fact]
        public void ParseJSONTestNotEmptyContacts()
        {
            JSONSerializ json = new JSONSerializ();

            json.ParseJSON("[{\"birthday\":\"0001-01-01\",\"emailAddress\":\"fede@gmail.com\",\"id\":0,\"name\":\"Fede\",\"phoneNumber\":\"+79193443062\"}]");
            Assert.NotEmpty(json.Contacts);
        }

        [Fact]
        public void ParseJSONgetContactNotNULL()
        {
            JSONSerializ json = new JSONSerializ();

            json.ParseJSONgetContact("{\"birthday\":\"0001-01-01\",\"emailAddress\":\"fede@gmail.com\",\"id\":0,\"name\":\"Fede\",\"phoneNumber\":\"+79193443062\"}");
            Assert.NotNull(json.Contact);
        }
    }
}
