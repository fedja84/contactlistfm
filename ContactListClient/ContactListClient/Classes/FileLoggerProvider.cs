﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace ContactListClient.Classes
{
    class FileLoggerProvider : ILoggerProvider
    {
        private string path;
        private LogLevel LogLevel;
        public FileLoggerProvider(string _path, LogLevel logLevel)
        {
            LogLevel = logLevel;
            path = _path;
        }
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger(path, LogLevel);
        }

        public void Dispose()
        {
        }
    }
}
