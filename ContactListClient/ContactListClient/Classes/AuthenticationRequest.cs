﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ContactListClient.Classes
{
    [DataContract]
    public class AuthenticationRequest
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Password { get; set; }


        public AuthenticationRequest(string userName, string password)
        {
            Name = userName;
            Password = password;    
        }
    }
}
