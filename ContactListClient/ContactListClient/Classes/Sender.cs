﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ContactListClient.Classes
{
    public class Sender
    {
        public string response = "";
        HttpClient client = new HttpClient();
        public async Task getContacts()
        {
            client.BaseAddress = new Uri("https://localhost:44328/api/contacts");
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            response = await client.GetStringAsync("https://localhost:44328/api/contacts");
        }

    }
}
