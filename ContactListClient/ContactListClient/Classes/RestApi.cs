﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace ContactListClient.Classes
{
    public class RestApi
    {
        private string Url;
        private string JWT;
        public string jwt
        { 
            set { JWT = value; }
        }
        private string request;
        public RestApi(string url)
        {
            Url = url;
        }
        private HttpClient httpClient;
        private HttpResponseMessage httpResponse;
        
        private StringContent httpContent;
        private HttpStatusCode ResultStatus;
        public HttpStatusCode resultStatus
        {
            get { return ResultStatus; }
        }

        public async Task<string> get()
        {
            string response = "";
            using (httpClient = new HttpClient())
            {
                prepareRequest("");
                httpResponse = await httpClient.GetAsync(Url);
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        response = await httpResponse.Content.ReadAsStringAsync();
                    }
                }
            }
            ResultStatus = httpResponse.StatusCode;
            return response;
        }

        public async Task<string> get(int id)
        {
            string response = "";
            using (httpClient = new HttpClient())
            {
                prepareRequest("");
                httpResponse = await httpClient.GetAsync($"{Url}//{id}");
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        response = await httpResponse.Content.ReadAsStringAsync();
                    }
                }
            }
            ResultStatus = httpResponse.StatusCode;
            return response;
        }
        public async Task<string> Post(string request)
        {
            string response = "";
            using (httpClient = new HttpClient())
            {
                prepareRequest(request);
                httpResponse = await httpClient.PostAsync(Url, httpContent);

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        response = await httpResponse.Content.ReadAsStringAsync();
                    }
                }
            }
            ResultStatus = httpResponse.StatusCode;
            return response;
        }

        public async Task<string> Put(string request)
        {
            string response = "";
            using (httpClient = new HttpClient())
            {
                prepareRequest(request);
                httpResponse = await httpClient.PutAsync(Url, httpContent);

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        response = await httpResponse.Content.ReadAsStringAsync();
                    }
                }
            }
            ResultStatus = httpResponse.StatusCode;
            return response;
        }

        public async Task<string> delete(int id)
        {
            string response = "";
            using (httpClient = new HttpClient())
            {
                prepareRequest("");
                httpResponse = await httpClient.DeleteAsync($"{Url}/{id}");
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        response = await httpResponse.Content.ReadAsStringAsync();
                    }
                }

            }
            ResultStatus = httpResponse.StatusCode;
            return response;
        }

        private void prepareRequest(string request)
        {
            if (!string.IsNullOrEmpty(request))
            {
                httpContent = new StringContent(request, Encoding.UTF8, "application/json");
            }
            else
            {
                httpContent = null;
            }
            if (!string.IsNullOrEmpty(JWT))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);
            }
        }
    }
}
