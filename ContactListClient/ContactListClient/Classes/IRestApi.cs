﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContactListClient.Classes
{
    public interface IRestApi
    {
        public Task<string> get();
        public Task<string> get(int id);
        public Task<string> post(string request);
        public Task<string> put(string request);
        public Task<string> delete(int id);
    }
}
