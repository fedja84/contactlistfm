﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ContactListClient.Classes
{
    [DataContract]
    public class Contact
    {
        [DataMember(Name="id")]
        public int Id { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }
        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }
        
        private DateTime birthday;
        [DataMember(Name = "birthday")]
        public string Birthday
        {
            get
            {
                return birthday.ToString("yyyy-MM-dd");// Substring(0, 10);
            }
            set
            {
                if (!DateTime.TryParse(value, out birthday))
                {
                    birthday = DateTime.MinValue;   
                }
                
            }
        }


        public Contact()
        { }

        public Contact(int id, string name, string phoneNumber, string emailAddress)
        {
            Id = id;
            Name = name;
            PhoneNumber = phoneNumber;
            EmailAddress = emailAddress;
        }
        public Contact(string name, string phoneNumber, string emailAddress)
        {
            Id = 0; 
            Name = name;
            PhoneNumber = phoneNumber;
            EmailAddress = emailAddress;
        }
    }
}
