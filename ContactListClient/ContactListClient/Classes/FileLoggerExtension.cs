﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace ContactListClient.Classes
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath, LogLevel logLevel)
        {
            filePath += $"_{DateTime.Now.ToString("yyyy-MM-dd")}.txt";
            factory.AddProvider(new FileLoggerProvider(filePath, logLevel));
            return factory;
        }
    }
}
