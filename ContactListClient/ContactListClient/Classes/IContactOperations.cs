﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContactListClient.Classes
{
    public interface IContactOperations
    {
        public Task<List<Contact>> getContacts();
        public Task<Contact> getContact(int contactId);
        public Task<Contact> addContact(Contact newContact);
        public Task<Contact> updateContact(Contact contactForUpdate);
        public Task<Contact> deleteContact(Contact contactForDel);
    }
}
