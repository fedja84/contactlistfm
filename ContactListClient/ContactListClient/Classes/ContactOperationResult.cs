﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactListClient.Classes
{
    public class ContactOperationResult
    {
        private bool Success;
        public bool success
        {
            get { return Success; }
        }
        private string Message;
        public string message
        {
            get { return Message; }
        }
        public void setResult(bool isSuccess, string msg)
        {
            Success = isSuccess;
            Message = msg;
        }
    }
}
