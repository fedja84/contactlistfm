﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactListClient.Classes
{
    public class NeedAuthorizationExcepetion : Exception
    {
        public NeedAuthorizationExcepetion(string Message)
            : base(Message)
        { }
    }
}
