﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;

namespace ContactListClient.Classes
{
    public class JSONSerializ
    {
        public string JsonString { get; set; }
        public List<Contact> Contacts { get; set; }
        public Contact Contact { get; set; }

        public JSONSerializ()
        {
            Contacts = new List<Contact>();
        }
        public void GetJson(Contact contact)
        {
            JsonString = "";
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(contact.GetType());
            using (MemoryStream stream = new MemoryStream())
            {
                jsonFormatter.WriteObject(stream, contact);
                stream.Position = 0;
                StreamReader sr = new StreamReader(stream);
                JsonString = sr.ReadToEnd();
                sr.Close();
            }
        }

        public void ParseJSON(string json)
        {
            Contacts = new List<Contact>();
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(Contacts.GetType());
                Contacts = jsonFormatter.ReadObject(ms) as List<Contact>;
            }
        }
        public void ParseJSONgetContact(string json)
        {
            Contacts = null;
            Contact = new Contact();
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(Contact.GetType());
                Contact = jsonFormatter.ReadObject(ms) as Contact;
            }
        }
    }
}
