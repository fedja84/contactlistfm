﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContactListClient.Classes
{
    public class ContactOperationRESTAPI : IContactOperations
    {
        private bool NeedAuthorization;
        public bool needAuthorization
        {
            set { NeedAuthorization = value; }
            get { return NeedAuthorization; }
        }
        private JSONSerializ json = new JSONSerializ();
        private RestApi rest;

        public ContactOperationRESTAPI(string url)
        {
            rest = new RestApi(url);
        }
        public void setJWT(string jwt)
        {
            rest.jwt = jwt;
        }
        public ContactOperationResult resultLastOperation = new ContactOperationResult();

        public async Task<List<Contact>> getContacts()
        {
            string jsoneResponse;
            jsoneResponse = await rest.get();
            if (rest.resultStatus == System.Net.HttpStatusCode.OK)
            {
                json.ParseJSON(jsoneResponse);
                needAuthorization = false;
                resultLastOperation.setResult(true, "success");
            }
            else
            {
                resultLastOperation.setResult(false, $"{(int)rest.resultStatus} {rest.resultStatus}");
                switch (rest.resultStatus)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        needAuthorization = true;
                        break;
                }
            }
            return json.Contacts;
        }

        public async Task<Contact> getContact(int contactId)
        {
            string jsoneResponse;
            jsoneResponse = await rest.get(contactId);
            if (rest.resultStatus == System.Net.HttpStatusCode.OK)
            {
                json.ParseJSONgetContact(jsoneResponse);
                needAuthorization = false;
                resultLastOperation.setResult(true, "success");
            }
            else
            {
                resultLastOperation.setResult(false, $"{(int)rest.resultStatus} {rest.resultStatus}");
                switch (rest.resultStatus)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        needAuthorization = true;
                        break;
                }
            }
            return json.Contact;
        }

        public async Task<Contact> addContact(Contact newContact)
        {
            json.GetJson(newContact);
            string jsoneResponse;
            jsoneResponse = await rest.Post(json.JsonString);
            if (rest.resultStatus == System.Net.HttpStatusCode.OK)
            {
                json.ParseJSONgetContact(jsoneResponse);
                needAuthorization = false;
                resultLastOperation.setResult(true, "success");
            }
            else
            {
                resultLastOperation.setResult(false, $"{(int)rest.resultStatus} {rest.resultStatus}");
                switch (rest.resultStatus)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        needAuthorization = true;
                        break;
                }
            }
            return json.Contact;
        }

        public async Task<Contact> updateContact(Contact contactForUpdate)
        {
            json.GetJson(contactForUpdate);
            string jsoneResponse;
            jsoneResponse = await rest.Put(json.JsonString);
            if (rest.resultStatus == System.Net.HttpStatusCode.OK)
            {
                json.ParseJSONgetContact(jsoneResponse);
                needAuthorization = false;
                resultLastOperation.setResult(true, "success");
            }
            else
            {
                resultLastOperation.setResult(false, $"{(int)rest.resultStatus} {rest.resultStatus}");
                switch (rest.resultStatus)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        needAuthorization = true;
                        break;
                }
            }
            return json.Contact;
        }

        public async Task<Contact> deleteContact(Contact contactForDelete)
        {
            string jsoneResponse;
            jsoneResponse = await rest.delete(contactForDelete.Id);
            if (rest.resultStatus == System.Net.HttpStatusCode.OK)
            {
                json.ParseJSONgetContact(jsoneResponse);
                needAuthorization = false;
                resultLastOperation.setResult(true, "success");
            }
            else
            {
                resultLastOperation.setResult(false, $"{(int)rest.resultStatus} {rest.resultStatus}");
                switch (rest.resultStatus)
                {
                    case System.Net.HttpStatusCode.Unauthorized:
                        needAuthorization = true;
                        break;
                }
            }
            return json.Contact;
        }
    }
}
