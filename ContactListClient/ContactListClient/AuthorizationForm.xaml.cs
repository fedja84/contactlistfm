﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ContactListClient
{
    /// <summary>
    /// Логика взаимодействия для AuthorizationForm.xaml
    /// </summary>
    public partial class AuthorizationForm : Window
    {
        public bool accept = false;
        public AuthorizationForm()
        {
            InitializeComponent();
        }

        private void button_cancle_Click(object sender, RoutedEventArgs e)
        {
            accept = false;
            this.Close();
        }

        private void button_login_Click(object sender, RoutedEventArgs e)
        {
            accept = true;
            this.Close();
        }
    }
}
