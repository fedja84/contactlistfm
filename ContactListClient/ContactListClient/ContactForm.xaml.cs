﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ContactListClient.Classes;
using ContactListClient.Enums;

namespace ContactListClient
{
    /// <summary>
    /// Логика взаимодействия для AddContact.xaml
    /// </summary>
    public partial class ContactForm : Window
    {
        public Contact contact { get; set; }
        public bool accepted { get; set; }

        public ContactForm(ContactAction typeForm, Contact inContact)
        {
            InitializeComponent();
            switch (typeForm)
            {
                case ContactAction.addContact:
                    this.Title = "Add contact";
                    btn_accept.Content = "Add";
                    break;
                case ContactAction.updateContact:
                    this.Title = "Update contact";
                    btn_accept.Content = "Update";
                    contact = inContact;
                    if (inContact != null)
                    {
                        textBox_name.Text = contact.Name;
                        textBox_phone.Text = contact.PhoneNumber;
                        textBox_email.Text = contact.EmailAddress;
                        try
                        {
                            datePicker_birthday.SelectedDate = DateTime.Parse(contact.Birthday);
                        }
                        catch 
                        {
                            datePicker_birthday.SelectedDate = DateTime.Now;
                        }
                    }
                    break;
            }
        }

        private void btn_cancle_Click(object sender, RoutedEventArgs e)
        {
            accepted = false;
            this.Close();
        }

        private void btn_accept_Click(object sender, RoutedEventArgs e)
        {
            accepted = true;
            if (contact == null)
            {
                contact = new Contact(0, textBox_name.Text, textBox_phone.Text, textBox_email.Text);
                if (datePicker_birthday.SelectedDate != null)
                {
                    contact.Birthday = datePicker_birthday.SelectedDate.Value.ToString("yyyy-MM-dd");
                }
                else
                {
                    contact.Birthday = new DateTime(1753,1,1).ToString("yyyy-MM-dd");
                }
            }
            else
            {
                contact.Name = textBox_name.Text;
                contact.EmailAddress = textBox_email.Text;
                contact.PhoneNumber = textBox_phone.Text;
                contact.Birthday = datePicker_birthday.SelectedDate.Value.ToString("yyyy-MM-dd");
            }
            this.Close();
        }

        private void textBox_phone_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            long val;
            if (!long.TryParse(e.Text, out val) && e.Text != "+" && e.Text != "-" && e.Text != "(" && e.Text != ")")
            {
                e.Handled = true;
            }
        }

        private void textBox_phone_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
