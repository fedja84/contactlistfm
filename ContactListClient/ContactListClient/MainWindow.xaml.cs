﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using ContactListClient.Classes;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using ContactListClient.Enums;
using System.Configuration;
using System.Collections.Specialized;
using Microsoft.Extensions.Logging;

namespace ContactListClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Contact> Contacts;
        private string url = "";
        private string authUrl = "";
        private string JWT = "";
        private ILogger loger;
        private ContactOperationRESTAPI geterContact;
        private bool needAutharization = true;
        public MainWindow()
        {
            string pathUrlForContacts = "";
            string partUrlForAuthorization = "";
            int loglevel = 3;
            ILoggerFactory loggerFactory;
            InitializeComponent();

            NameValueCollection appSettings = ConfigurationManager.AppSettings;
            for (int i = 0; i < appSettings.Count; i++)
            {
                switch (appSettings.GetKey(i))
                { 
                    case "url":
                        url = appSettings[i];
                        break;
                    case "urlcontact":
                        pathUrlForContacts = appSettings[i];
                        break;
                    case "urlauth":
                        partUrlForAuthorization = appSettings[i];
                        break;
                    case "loglevel":
                        if (!int.TryParse(appSettings[i], out loglevel))
                        {
                            loglevel = 3;
                        }
                        break;
                }
            }
            authUrl = $"{url}{partUrlForAuthorization}";
            url += pathUrlForContacts;
            
            loggerFactory = new LoggerFactory();
            FileLoggerExtensions.AddFile(loggerFactory, System.IO.Path.Combine(Directory.GetCurrentDirectory(), "logClient"), (LogLevel)loglevel);
            loger = loggerFactory.CreateLogger("FileLogger");
            loger.LogInformation("==== Program started ====");
            loger.LogDebug($"Settings received:{Environment.NewLine}\turl={url}");
            geterContact = new ContactOperationRESTAPI(url);
        }

        private async Task<string> Authorization()
        {
            string token = "";
            loger.LogDebug("Start authorization");
            string authJSONRequest = "";
            AuthorizationForm authForm = new AuthorizationForm();
            authForm.ShowDialog();
            if (authForm.accept)
            {
                AuthenticationRequest authReaq = new AuthenticationRequest(authForm.textBox_uselName.Text, authForm.textBox_password.Password);
                loger.LogInformation($"User authorization '{authReaq.Name}'");
                DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(authReaq.GetType());

                using (MemoryStream stream = new MemoryStream())
                {
                    jsonFormatter.WriteObject(stream, authReaq);
                    stream.Position = 0;
                    StreamReader sr = new StreamReader(stream);
                    authJSONRequest = sr.ReadToEnd();
                    sr.Close();
                    loger.LogDebug($"authJSONRequest created. Length={authJSONRequest.Length}");
                }

                var httpContent = new StringContent(authJSONRequest, Encoding.UTF8, "application/json");
                using (var httpClient = new HttpClient())
                {
                    loger.LogDebug("Sending authorization request...");
                    var httpResponse = await httpClient.PostAsync(authUrl, httpContent);

                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        if (httpResponse.Content != null)
                        {
                            token = await httpResponse.Content.ReadAsStringAsync();
                            if (token.Length > 0)
                            {
                                loger.LogInformation("Authorization success key received");
                            }
                            else
                            {
                                loger.LogWarning($"Authorization response status success, but Key not received!");
                            }
                        }
                        else
                        {
                            loger.LogWarning($"Authorization response status success, but httpResponse.Content is null!");
                        }
                    }
                    else
                    {
                        string authResponse = httpResponse.StatusCode.ToString();
                        if (httpResponse.Content != null)
                        {
                            authResponse = await httpResponse.Content.ReadAsStringAsync();
                        }
                        loger.LogWarning($"Authorization response status {(int)httpResponse.StatusCode} {httpResponse.StatusCode.ToString()}\r\nResponse:{authResponse}");
                        MessageBox.Show($"Login Failed {(int)httpResponse.StatusCode} {authResponse}");
                    }
                }
            }
            else
            {
                loger.LogInformation("User refused to login");
                geterContact.needAuthorization = false;
            }
            JWT = token;
            return token;
        }

        private async void button_getContacts_Click(object sender, RoutedEventArgs e)
        {
            do
            {
                try
                {
                    //Contacts = await GetContactsAsync();
                    loger.LogDebug("Start get contacts...");
                    //Contacts = await sendRequest(TypeRequest.get, null);
                    Contacts = await geterContact.getContacts();
                    dataGrid_contacts.ItemsSource = Contacts;
                    if (geterContact.needAuthorization)
                    {
                        loger.LogInformation("Need authorization on getContacts");
                        geterContact.setJWT(await Authorization());
                        
                    }
                    else
                    {
                        if (geterContact.resultLastOperation.success)
                        {
                            loger.LogInformation("Contacts received, count=" + Contacts.Count);
                            refreshDataGrid();
                        }
                        else
                        {
                            geterContact.needAuthorization = false;
                            MessageBox.Show($"Contact List Not Received:{geterContact.resultLastOperation.message}", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            loger.LogError($"Contact List Not Received:{geterContact.resultLastOperation.message}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    geterContact.needAuthorization = false;
                    MessageBox.Show($"Error getting contacts:{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    loger.LogError($"Error getting contacts:{ex.ToString()}");
                }
            } while (geterContact.needAuthorization);
        }

        private async void button_addContact_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loger.LogDebug("Start add contact");
                ContactForm addContactWindow = new ContactForm(ContactAction.addContact, null);
                addContactWindow.ShowDialog();

                if (addContactWindow.accepted)
                {
                    do
                    {
                        //Contacts.AddRange(await addContactaddContact(addContactWindow.contact));
                        //Contacts.AddRange(await sendRequest(TypeRequest.post, addContactWindow.contact));
                        Contacts.Add(await geterContact.addContact(addContactWindow.contact));
                        if (geterContact.needAuthorization)
                        {
                            loger.LogInformation("Need authorization on addContact");
                            geterContact.setJWT(await Authorization());
                        }
                        else
                        {
                            if (geterContact.resultLastOperation.success)
                            {
                                loger.LogInformation("New contact added");
                                refreshDataGrid();
                            }
                            else
                            {
                                geterContact.needAuthorization = false;
                                MessageBox.Show($"Contact not added:{geterContact.resultLastOperation.message}", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                                loger.LogError($"Contact not added:{geterContact.resultLastOperation.message}");
                            }
                        }
                    }
                    while (geterContact.needAuthorization);
                }
                else
                {
                    loger.LogDebug("User did not add contact");
                }
            }
            catch (Exception ex)
            {
                geterContact.needAuthorization = false;
                MessageBox.Show($"Error add contact:{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                loger.LogError($"Error add contact:{ex.ToString()}");
            }
        }

        private async void button_updateContact_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_contacts.SelectedItem != null)
                {
                    loger.LogDebug($"Satrt update contact. Contact id:{(dataGrid_contacts.SelectedItem as Contact).Id}");
                    ContactForm updateContactForm = new ContactForm(ContactAction.updateContact, dataGrid_contacts.SelectedItem as Contact);
                    updateContactForm.ShowDialog();

                    if (updateContactForm.accepted && updateContactForm.contact != null)
                    {
                        do
                        {
                            Contact updatedContact;
                            updatedContact = await geterContact.updateContact(updateContactForm.contact);
                            if (geterContact.needAuthorization)
                            {
                                loger.LogInformation("Need authorization on update contact");
                                geterContact.setJWT(await Authorization());
                            }
                            else
                            {
                                if (geterContact.resultLastOperation.success)
                                {
                                    int index = Contacts.FindIndex(p => p.Id == updatedContact.Id);
                                    if (index >= 0)
                                    {
                                        Contacts[index].Name = updatedContact.Name;
                                        Contacts[index].PhoneNumber = updatedContact.PhoneNumber;
                                        Contacts[index].EmailAddress = updatedContact.EmailAddress;
                                        Contacts[index].Birthday = updatedContact.Birthday;
                                    }
                                    loger.LogInformation($"Contact updated Contact id:{(dataGrid_contacts.SelectedItem as Contact).Id}");
                                    refreshDataGrid();
                                }
                                else
                                {
                                    geterContact.needAuthorization = false;
                                    MessageBox.Show($"Contact not changed:{geterContact.resultLastOperation.message}", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                                    loger.LogError($"Contact not changed:{geterContact.resultLastOperation.message}");
                                }
                            }
                        }
                        while (geterContact.needAuthorization);
                    }
                    else
                    {
                        if (!updateContactForm.accepted)
                        {
                            loger.LogInformation("User did not update contact");
                        }
                        else
                        {
                            if (updateContactForm.contact == null)
                            {
                                loger.LogWarning("Contact for update is NULL");
                            }
                        }
                    }
                }
                else
                {
                    loger.LogWarning("Unable to change contact because contact not selected");
                }
            }
            catch (Exception ex)
            {
                geterContact.needAuthorization = false;
                MessageBox.Show($"Error update contact:{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                loger.LogError($"Error update contact:{ex.ToString()}");
            }
        }
        
        private async void button_delContact_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loger.LogInformation("Start delete contacts");
                if (dataGrid_contacts.SelectedItem != null)
                {
                    string message;

                    if (dataGrid_contacts.SelectedItems.Count > 1)
                    {
                        message = "Are you sure you want to delete the selected contacts?";
                    }
                    else
                    {
                        message = $"Are you sure you want to delete the contact '{(dataGrid_contacts.SelectedItem as Contact).Name} '?";
                    }

                    if (MessageBox.Show(message, "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        loger.LogInformation($"{dataGrid_contacts.SelectedItems.Count} contacts will be deleted");
                        foreach (var item in dataGrid_contacts.SelectedItems)
                        {
                            do
                            {
                                Contact contactForDel = item as Contact;
                                loger.LogDebug($"Delete contact. Contact id:{contactForDel.Id}");
                                //await sendRequest(TypeRequest.delete, contactForDel);
                                await geterContact.deleteContact(contactForDel);
                                if (geterContact.needAuthorization)
                                {
                                    loger.LogInformation("Need authorization on delete contact");
                                    geterContact.setJWT(await Authorization());
                                }
                                else
                                {
                                    if (geterContact.resultLastOperation.success)
                                    {
                                        int indexDelContact = Contacts.FindIndex(p => p.Id == contactForDel.Id);
                                        if (indexDelContact >= 0)
                                        {
                                            Contacts.RemoveAt(indexDelContact);
                                            loger.LogInformation($"Contact deleted. Contact id:{contactForDel.Id}");
                                        }
                                        else
                                        {
                                            loger.LogWarning($"It is not possible to remove a contact from the list as not found. Contact id:{contactForDel.Id}");
                                        }
                                    }
                                    else
                                    {
                                        geterContact.needAuthorization = false;
                                        MessageBox.Show($"Contact not deleted:{geterContact.resultLastOperation.message}", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        loger.LogError($"Contact not deleted:{geterContact.resultLastOperation.message}");
                                    }
                                }
                            }
                            while(geterContact.needAuthorization);
                        }
                        refreshDataGrid();
                    }
                }
                else
                {
                    loger.LogWarning("Unable to delete contact, because contacts not selected");
                }
            }
            catch (Exception ex)
            {
                geterContact.needAuthorization = false;
                MessageBox.Show($"Error delete contact:{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                loger.LogError($"Error delete contact:{ex.ToString()}");
            }
        }

        
        private void dataGrid_contacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            button_updateContact.IsEnabled = button_delContact.IsEnabled = true;
        }
        private void dataGrid_contacts_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            button_updateContact.IsEnabled = dataGrid_contacts.SelectedItems.Count == 1;
            button_delContact.IsEnabled = dataGrid_contacts.SelectedItems.Count > 0;
        }

        private void refreshDataGrid()
        {
            dataGrid_contacts.ItemsSource = null;
            dataGrid_contacts.ItemsSource = Contacts;

            button_updateContact.IsEnabled = button_delContact.IsEnabled = false;
        }


        // ВАРИАНТЫ //

        private async Task<List<Contact>> sendRequest(TypeRequest type, Contact contact)
        {
            loger.LogDebug($"Send request.{Environment.NewLine}\t Type:{type.ToString()}, {(contact == null ? "contact is null" : "contact is not null")}");
            bool waitOneContact = false;
            string jsonRequest = "";
            JSONSerializ json = new JSONSerializ();
            if (contact != null)
            {
                json.GetJson(contact);
                jsonRequest = json.JsonString;
                loger.LogDebug($"jsonRequest='{jsonRequest}'");
            }
            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

                HttpResponseMessage httpResponse = null;
                loger.LogDebug($"Sending request...");
                switch (type)
                {
                    case TypeRequest.get:
                        httpResponse = await httpClient.GetAsync(url);
                        break;
                    case TypeRequest.post:
                        waitOneContact = true;
                        httpResponse = await httpClient.PostAsync(url, httpContent);
                        break;
                    case TypeRequest.put:
                        waitOneContact = true;
                        httpResponse = await httpClient.PutAsync(url, httpContent);
                        break;
                    case TypeRequest.delete:
                        httpResponse = await httpClient.DeleteAsync($"{url}/{contact.Id.ToString()}");
                        break;
                }

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        loger.LogDebug($"httpResponseStatus success");
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();
                        if (waitOneContact)
                        {
                            responseContent = $"[{responseContent}]";
                        }
                        loger.LogDebug($"responseContent:'{responseContent}'");
                        json.ParseJSON(responseContent);
                        loger.LogDebug($"Response parsed");
                        needAutharization = false;
                    }
                    else
                    {
                        loger.LogWarning($"httpResponseStatus success, but httpResponse.Content is null.{Environment.NewLine}Type:{type.ToString()},{Environment.NewLine}{(contact == null ? "contact is null" : "contact is not null")},{Environment.NewLine}jsonRequest='{jsonRequest}'");
                    }
                }
                else
                {
                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            needAutharization = true;
                            break;
                        default:
                            loger.LogWarning($"httpResponseStatus:{(int)httpResponse.StatusCode}  {httpResponse.StatusCode.ToString()}{Environment.NewLine}Type:{type.ToString()}{Environment.NewLine}{(contact == null ? "contact is null" : "contact is not null")},{Environment.NewLine}jsonRequest='{jsonRequest}'");
                            break;
                    }
                }
            }
            return json.Contacts;
        }

        private async Task<List<Contact>> GetContactsAsync()
        {

            /*var serializer = new DataContractJsonSerializer(typeof(List<Contact>));
            using (HttpClient client = new HttpClient())
            {
                var streamTask = await client.GetStreamAsync(url);
                Contacts = serializer.ReadObject(streamTask) as List<Contact>;
            }

            return Contacts;*/
            if (string.IsNullOrEmpty(JWT))
            {
                needAutharization = true;
                return null;
            }

            JSONSerializ json = new JSONSerializ();

            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + JWT);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);
                var httpResponse = await httpClient.GetAsync(url);

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();
                        json.ParseJSON(responseContent);
                        needAutharization = false;
                    }
                }
                else
                {
                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            needAutharization = true;
                            break;
                    }
                }
            }
            return json.Contacts.ToList();
        }

        private async Task<bool> delContact(int id)
        {
            bool result = false;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);

                var httpResponse = httpClient.DeleteAsync($"{url}/{id.ToString()}");
                var responseContent = await httpResponse.Result.Content.ReadAsStringAsync();
                if (httpResponse.Result.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                    needAutharization = false;
                }
                else
                {
                    switch (httpResponse.Result.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            needAutharization = true;
                            break;
                    }
                }
                //result = httpResponse.Result.IsSuccessStatusCode;
            }
            return result;
        }

        private async Task<Contact> updateContact(Contact contact)
        {
            JSONSerializ json = new JSONSerializ();
            json.GetJson(contact);
            var httpContent = new StringContent(json.JsonString, Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);
                var httpResponse = await httpClient.PutAsync(url, httpContent);

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();
                        json.ParseJSONgetContact(responseContent);
                        needAutharization = false;
                    }
                }
                else
                {
                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            needAutharization = true;
                            break;
                    }
                }
            }
            return json.Contact;
        }

        private async Task<List<Contact>> addContactaddContact(Contact newContact)
        {
            JSONSerializ json = new JSONSerializ();
            json.GetJson(newContact);
            var httpContent = new StringContent(json.JsonString, Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JWT);
                var httpResponse = await httpClient.PostAsync(url, httpContent);

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();
                        json.ParseJSON($"[{responseContent}]");
                        needAutharization = false;
                    }
                }
                else
                {
                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            needAutharization = true;
                            break;
                    }
                }
            }
            return json.Contacts.ToList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            loger.LogInformation("==== Program shutdown ====");
        }
    }
}
