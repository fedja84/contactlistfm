﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactListClient.Enums
{
    public enum ContactAction
    {
        addContact,
        updateContact
    }
}
