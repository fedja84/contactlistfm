﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactListClient.Enums
{
    public enum TypeRequest
    {
        get,
        put,
        post,
        delete
    }
}
